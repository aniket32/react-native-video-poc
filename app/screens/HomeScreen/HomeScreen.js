import React from 'react';
import {View, SafeAreaView, Text} from 'react-native';
import {VideoPlayer} from '../../components';
import styles from './HomeScreenStyles';

function HomeScreen(props) {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'black'}}>
      <View style={styles().container}>
        <VideoPlayer />
      </View>
    </SafeAreaView>
  );
}

export default HomeScreen;
