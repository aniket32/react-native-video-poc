import {StyleSheet} from 'react-native';

const styles = (props = {}) =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
  });

export default styles;
