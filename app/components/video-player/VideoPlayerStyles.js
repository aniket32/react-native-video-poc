import {StyleSheet, Dimensions} from 'react-native';

const screenHeight = Dimensions.get('screen').height;
const screenWidth = Dimensions.get('screen').width;

const styles = (props = {}) =>
  StyleSheet.create({
    container: {
      overflow: 'hidden',
      backgroundColor: '#000',
      flex: 1,
      alignSelf: 'center',
      justifyContent: 'center',
    },
    videoContainer: {
      // flex: 1,
      height: screenHeight,
      width: screenWidth
    },
    fullScreen: {
      top: 0,
      bottom: 0,
      right: 0,
      left: 0,
      position: 'absolute',
      overflow: 'hidden',
    },
    pausePlayBtn: {
      top: '45%',
      alignSelf: 'center',
    },
  });

export default styles;
