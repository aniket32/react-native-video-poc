import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Pressable,
  LayoutAnimation,
  UIManager,
  Platform,
  ActivityIndicator
} from 'react-native';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './VideoPlayerStyles';

Icon.loadFont();

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

function PausePlayButton({paused, setPaused}) {
  return (
    <View style={{flex: 1}}>
      <TouchableOpacity
        onPress={() => setPaused(!paused)}
        style={styles().pausePlayBtn}>
        <Icon
          name={paused ? 'play-circle-outline' : 'pause-circle-outline'}
          size={60}
          color="#FFFFFF"
        />
      </TouchableOpacity>
    </View>
  )
}

function Loader({visible}) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size="large" animating={visible} />
    </View>
  )
}

function VideoPlayer() {
  const [paused, setPaused] = useState(false);
  const [controlsVisibility, setControlsVisibility] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [resizeMode, setResizeMode] = useState('contain');

  video: Video

  useEffect(() => {
    if (controlsVisibility) {
      setTimeout(() => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        setControlsVisibility(false);
      }, 5000);
    }
  }, [controlsVisibility]);

  const onEndReached = () => {
    setPaused(true);
    setControlsVisibility(true);
  }

  return (
    <View style={styles().container}>
        <Video
          ref={(ref: Video) => { video = ref }}
          source={{
            uri:
              'https://pm-sample.s3.ap-south-1.amazonaws.com/the-human-eye-closeup.mp4',
          }}
          paused={paused}
          resizeMode={resizeMode}
          fullscreen={true}
          fullscreenOrientation={'landscape'}
          fullscreenOrientation={'landscape'}
          onLoadStart={() => setLoading(true)}
          onLoad={() => setLoading(false)}
          onEnd={onEndReached}
          style={styles().videoContainer}
          repeat={false}
        />
        <Pressable
          style={styles().fullScreen}
          onPress={() => {
            LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
            setControlsVisibility(!controlsVisibility);
          }}>
            {isLoading ? <Loader visible={isLoading} /> : controlsVisibility && 
              (
                <>
                  <PausePlayButton paused={paused} setPaused={(val) => setPaused(val)}/>
                </>
              )
            }
          </Pressable>
    </View>
  );
}

export default VideoPlayer;
